<?php

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \RestApis\Blockchain\Constants;
use App\Entity as Entity;
use App\Form as Form;

class ExchangeController extends AbstractController
{
    /**
     * @Route("/exchange/dashboard", name="exchangeHome")
     */
    public function exchangeHome(): Response
    {
        return $this->render('exchange/dashboard.html.twig', [
            'controller_name' => 'ExchangeController',
        ]);
    }
    /**
     * @Route("/exchange/wallets", name="exchangeWallets")
     */
    public function exchangeWallets(): Response
    {
        $wallets = $this->getDoctrine()->getManager()->getRepository('App:Wallet')
            ->findBy(['user'=>$this->getUser()],['sort'=>'ASC']);

        return $this->render('exchange/wallets.html.twig', [
            'wallets' => $wallets,
        ]);
    }

    /**
     * @Route("/exchange/IRT/input", name="exchangeIRTInput")
     */
    public function IRTInput(Request $request){

        $transaction = new Entity\Transaction();
        $form = $this->createForm(Form\TransactionInputType::class,$transaction);

        return $this->render('exchange/IRTCharge.html.twig',
        [
            'form'=>$form->createView()
        ]);

    }
}
