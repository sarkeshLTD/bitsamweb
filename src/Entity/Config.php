<?php

namespace App\Entity;

use App\Repository\ConfigRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ConfigRepository::class)
 */
class Config
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $siteName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smsAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smsToken;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $ConfigCode;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiteName(): ?string
    {
        return $this->siteName;
    }

    public function setSiteName(?string $siteName): self
    {
        $this->siteName = $siteName;

        return $this;
    }

    public function getSmsAddress(): ?string
    {
        return $this->smsAddress;
    }

    public function setSmsAddress(?string $smsAddress): self
    {
        $this->smsAddress = $smsAddress;

        return $this;
    }

    public function getSmsToken(): ?string
    {
        return $this->smsToken;
    }

    public function setSmsToken(?string $smsToken): self
    {
        $this->smsToken = $smsToken;

        return $this;
    }

    public function getConfigCode(): ?string
    {
        return $this->ConfigCode;
    }

    public function setConfigCode(?string $ConfigCode): self
    {
        $this->ConfigCode = $ConfigCode;

        return $this;
    }
}
