<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GeneralController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(): Response
    {
        return $this->render('general/home.html.twig', [

        ]);
    }
    /**
     * @Route("/page/{id}", name="staticPages")
     */
    public function staticPages($id='no'): Response
    {
        return $this->render('general/home.html.twig', [
            'controller_name' => 'GeneralController',
        ]);
    }

}
