<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserMobileVerifyType;
use App\Form\UserRegisterType;
use App\Form\UserResetPasswordGetNumberType;
use App\Form\UserResetPasswordVerifyType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Service;

class UserController extends AbstractController
{
    /**
     * function to generate random strings
     * @param 		int 	$length 	number of characters in the generated string
     * @return 		string	a new string is created with random characters of the desired length
     */
    private function RandomString($length = 32) {
        return substr(str_shuffle(str_repeat($x='0123456789', ceil($length/strlen($x)) )),1,$length);
    }

    /**
     * @Route("/login/{msg}", name="app_login")
     */
    public function login($msg = 0,AuthenticationUtils $authenticationUtils ,UserPasswordEncoderInterface $passwordEncoder): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        return $this->render('user/login.html.twig', [
            'error' => $error,
            'msg' => $msg
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function app_register(
        AuthenticationUtils $authenticationUtils ,
        UserPasswordEncoderInterface $passwordEncoder,
        Request $request,
        Service\Notification $notification,
        TranslatorInterface $translator
    ): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }
        $user = new \App\Entity\User();
        $form = $this->createForm(UserRegisterType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $form->get('password')->getData());
            $user->setPassword($password);
            $user->setRoles(['ROLL_USER']);
            $user->setSmsVerify($this->RandomString(6));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $notification->sendSMS($user->getMobile(),$translator->trans('active code is %code%',['%code%'=>$user->getSmsVerify()])  );
            return $this->redirectToRoute('app_register_mobile',['id'=>$user->getId()]);
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        return $this->render('user/register.html.twig', [
            'error' => $error,
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/verify_mobile/{id}", name="app_register_mobile")
     * @ParamConverter("mobile", class="App:User")
     */
    public function app_register_mobile(User $user,TranslatorInterface $translator,AuthenticationUtils $authenticationUtils ,UserPasswordEncoderInterface $passwordEncoder,Request $request): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }
        $em = $this->getDoctrine()->getManager();
        $verifySMS = $user->getSmsVerify();
        $user->setSmsVerify('');
        $form = $this->createForm(UserMobileVerifyType::class, $user);
        $form->get('smsVerify')->setData('');

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if($user->getSmsVerify() == $verifySMS){
                $user->setMobileVerify(true);
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('app_login',['msg'=>'mobile-verify']);
            }
            $form->addError(new FormError($translator->trans('sms_verify_incerrect')));
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        return $this->render('user/mobile_verify.html.twig', [
            'error' => $error,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/reset-password", name="app_reset_password")
     */
    public function app_reset_password(
        Request $request,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        Service\Notification $notification
    ): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }
        $message = ['message'=>'test'];
        $form = $this->createFormBuilder($message)
            ->add('mobile',TelType::class)
            ->add('submit', SubmitType::class,['label'=>'ثبت'])
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $mobileNum = $form->get('mobile')->getData();
            $userObject = $entityManager->getRepository('App:User')->findOneBy(['mobile'=>$mobileNum]);
            if($userObject){
                $userObject->setSmsVerify($this->RandomString(6));
                $entityManager->persist($userObject);
                $entityManager->flush();
                $notification->sendSMS($userObject->getMobile(),$translator->trans('forget_password_%code%',['%code%'=>$userObject->getSmsVerify()])  );
                return $this->redirectToRoute('app_reset_password_verify_mobile',['id'=>$userObject->getId()]);
            }
            $form->addError(new FormError($translator->trans('mobile_not_found')));
        }

        return $this->render('user/resetPassword.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("reset-password/verify_mobile/{id}", name="app_reset_password_verify_mobile")
     * @ParamConverter("mobile", class="App:User")
     */
    public function app_reset_password_verify_mobile(
        User $user,
        TranslatorInterface $translator,
        UserPasswordEncoderInterface $passwordEncoder,
        Service\Notification $notification,
        Request $request
    ): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('home');
        }
        $em = $this->getDoctrine()->getManager();
        $verifySMS = $user->getSmsVerify();
        $user->setSmsVerify('');
        $form = $this->createForm(UserResetPasswordVerifyType::class, $user);
        $form->get('smsVerify')->setData('');

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if($user->getSmsVerify() == $verifySMS){
                $password = $this->RandomString(8);
                $user->setPassword($passwordEncoder->encodePassword($user,$password));
                $em->persist($user);
                $em->flush();
                $notification->sendSMS($user->getMobile(),$translator->trans('your_new_password_%code%',['%code%'=>$password]));
                return $this->redirectToRoute('app_login',['msg'=>'reset-password']);
            }
            $form->addError(new FormError($translator->trans('sms_verify_incerrect')));
        }

        return $this->render('user/resetPasswordVerify.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/profile", name="app_user_profile")
     */
    public function app_user_profile(
        Request $request,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        Service\Notification $notification
    ): Response
    {
        return $this->render('user/profile.html.twig', [
        ]);
    }

}
