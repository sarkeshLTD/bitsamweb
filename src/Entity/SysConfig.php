<?php

namespace App\Entity;

use App\Repository\SysConfigRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SysConfigRepository::class)
 */
class SysConfig
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $configCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smsApiAdr;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $smsToken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cryptoAPI;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConfigCode(): ?string
    {
        return $this->configCode;
    }

    public function setConfigCode(string $configCode): self
    {
        $this->configCode = $configCode;

        return $this;
    }

    public function getSmsApiAdr(): ?string
    {
        return $this->smsApiAdr;
    }

    public function setSmsApiAdr(?string $smsApiAdr): self
    {
        $this->smsApiAdr = $smsApiAdr;

        return $this;
    }

    public function getSmsToken(): ?string
    {
        return $this->smsToken;
    }

    public function setSmsToken(string $smsToken): self
    {
        $this->smsToken = $smsToken;

        return $this;
    }

    public function getCryptoAPI(): ?string
    {
        return $this->cryptoAPI;
    }

    public function setCryptoAPI(?string $cryptoAPI): self
    {
        $this->cryptoAPI = $cryptoAPI;

        return $this;
    }
}
