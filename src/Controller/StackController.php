<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StackController extends AbstractController
{
    /**
     * @Route("/stack", name="stack")
     */
    public function index(): Response
    {
        return $this->render('stack/index.html.twig', [
            'controller_name' => 'StackController',
        ]);
    }
}
