<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\BrowserKit\CookieJar;

use App\Entity;

class Log
{
    protected $em;

    function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function create($user,$title,$des,$request){
        $log = new Entity\Log();
        $log->setUser($user);
        $log->setTitle($title);
        $log->setDes($des);
        $log->setDateSubmit(time());
        $log->setIP($request->getClientIP());
        $this->em->persist($log);
        $this->em->flush();
    }
}