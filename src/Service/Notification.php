<?php
namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\BrowserKit\CookieJar;

use App\Entity;

class Notification
{
    protected $em;

    function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function sendSMS($number,$body) : bool{
        $config = $this->em->getRepository('App:SysConfig')->findOneBy(['configCode'=>0]);
        $des = $body ;

        $webServiceURL  = $config->getSmsApiAdr();
        $req= new \App\DataObject\SendSms();
        $req-> SmsBody  =$des;
        $req-> Mobiles = [$number];

        $ch = curl_init($webServiceURL);
        $jsonDataEncoded = json_encode($req);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $header =array('authorization: BASIC APIKEY:'. $config->getSmsToken(),'Content-Type: application/json;charset=utf-8');
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        $result = curl_exec($ch);
        $res = json_decode($result);
        curl_close($ch);
        return true;
    }

    public function sendNotification($user,$des,$route,$params) : void
    {
        $notifi = new Entity\Notification();
        $notifi->setUser($user);
        $notifi->setDes($des);
        $notifi->setRouteName($route);
        $notifi->setRouteParams($params);
        $notifi->setDateSubmit(time());
        $notifi->setView(false);
        $this->em->persist($notifi);
        $this->em->flush();
    }

    public function getNotification($user,$count = 0 ){
        $notifis = $this->em->getRepository('App:Notification')->findBy(['user'=>$user]);
        return $notifis;
    }

}